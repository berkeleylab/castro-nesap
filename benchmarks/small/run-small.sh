#!/bin/bash
#SBATCH --account=m1759
#SBATCH --partition=debug
#SBATCH --constraint="quad,cache"
#SBATCH --nodes=1
#SBATCH --time=00:10:00
#SBATCH --job-name=starlord-small
#SBATCH --wait-all-nodes=1
#SBATCH --output=starlord-small.%j.out
num_nodes=$SLURM_JOB_NUM_NODES
ht_node=4				# needs to match above
c_node=68				# default cores per node
t_node=$(( c_node * ht_node ))          # total threads per node
t_total=$(( num_nodes * t_node ))       # total threads

# support for IPM
export IPM_REPORT=full
export IPM_LOG=full
export IPM_LOGDIR=.

#module load craype-hugepages2M

debug="false"
ht=4			# default hyperthreads per core
m=0 			# default numa memory mode
n=17			# default number of ranks per node
t=0			# derive threads
while [ "$#" -ge 1 ] ; do
  case "$1" in
    "--help" ) 	          shift 1;;
    "--debug" )           debug=true; shift 1;;
    "-n" | "--ranks" )    n=$2; shift 2;;
    "-t" | "--threads" )  t=$2; shift 2;;
    "-ht" )  		  ht=$2; shift 2;;
    "-m" )  		  m=$2; shift 2;;
    *)                    break;;
  esac
done

n=$(( n * num_nodes ))

# set the executable and input file
exe=Castro3d.intel.mic-knl.MPI.OMP.ex
inputs=inputs.small
if [ "$debug" == "false" ]; then 
  sbcast -f $exe /tmp/$exe
fi

# check for hyper threads
if [ $ht -gt $ht_node ]; then
  echo "ERROR: too many hyperthreads specified, exiting"
  exit
fi

export OMP_PLACES=threads
export OMP_PROC_BIND=spread
c=$(( t_total / n ))		# cores per rank
if [ $t -eq 0 ]; then		# t not set on command line
  t=$(( c / ht_node ))
  if [ $t -eq 0 ]; then		# Using MPI ranks on hyperthreads
    t=1
  elif [ $ht -gt 1 ]; then	# use hyperthreading
    export OMP_PLACES=cores"(${t})"
    (( t *= ht ))
    export OMP_PROC_BIND=close
  fi
fi
if [ $t -gt $c ]; then 	# t set, but check for sanity
  echo "ERROR: too many threads per rank specified, $t > $c, exiting"
  exit
fi 
export OMP_NUM_THREADS=$t

echo -n "$0: "
echo -n "OMP_PLACES=$OMP_PLACES; "
echo -n "OMP_PROC_BIND=$OMP_PROC_BIND; "
echo -n "OMP_NUM_THREADS=$OMP_NUM_THREADS; "
command="srun -n $n -c $c --cpu_bind=cores --mem_bind=map_mem:$m /tmp/$exe"
echo $command
if [ "$debug" == "false" ]; then 
  $command $inputs
fi

